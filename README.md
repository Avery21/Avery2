# Hi, I’m Avery 👋 <sub><sup>_(full name: Justin "Avery" Chan)_</sup></sub>

I’m currently a computer science and data science double major at the University of Wisconsin-Madison. This summer I am working as a developer intern at [Halo Science](https://www.halo.science/).

<div align="center">
  <a href="https://www.linkedin.com/in/avery2"><img alt="LinkedIn" src="https://img.shields.io/badge/Justin_Chan-%230077B5.svg?style=flat&logo=linkedin&logoColor=white"/></a>
  <a href="mailto:justinaverychan@gmail.com"><img alt="Email" src="https://img.shields.io/badge/justinaverychan@gmail.com-D14836?style=flat&logo=gmail&logoColor=white"/></a>
  <a href="https://www.averychan.site"><img alt="Website" src="https://img.shields.io/website?down_color=lightgrey&down_message=offline&label=averychan.site&up_color=green&up_message=online&url=https%3A%2F%2Fwww.averychan.site"/></a>
    <a href="https://www.averychan.site/normal-resume/Justin_Avery_Chan_Resume.pdf"><img alt="Resume" src="https://img.shields.io/badge/Resume_(last_updated)-Jul_2021-green"/></a>
</div><br/>

<details>
<summary>📈 Recent Activity <sub><sup>(last updated Saturday, August 14th 2021)</sup></sub></summary>

<h4>

```
🍴 Forked Avery2/iampavangandhi from iampavangandhi/iampavangandhi
💪 Opened PR #19 in jctaveras/heroku-deploy
🍴 Forked Avery2/heroku-deploy from jctaveras/heroku-deploy
💪 Opened PR #640 in abhisheknaiidu/awesome-github-profile-readme
💪 Opened PR #1 in umcody/umcody
🍴 Forked Avery2/umcody from umcody/umcody
🍴 Forked Avery2/awesome-github-profile-readme from abhisheknaiidu/awesome-github-profile-readme
```

</h4>

</details>

<details>
  <summary>💻 GitHub Stats</summary>
  <br/>
  <div align="center">
    <a href="https://github.com/Avery2" target="__blank">
      <img align="center" src="https://github-readme-stats.vercel.app/api?username=avery2&count_private=true&show_icons=true&hide=issues" />
    </a>
  </div>
  <br/>
</details>

<details>
  <summary>🛠 Tools for this README</summary><br/>

<div align="center">
<h4>

| Tool | Link |
|---|---:|
| Sheilds.io for the badges | [link](https://shields.io) |
| Profile-readme for recent activity | [link](https://github.com/actions-js/profile-readme) |
| Github-readme-stats for the GitHub stats summary | [link](https://github.com/anuraghazra/github-readme-stats) |
| Productive-box for pinned gist (below) of commit times | [link](https://github.com/maxam2017/productive-box) ||

</h4>
</div>
  
</details>
